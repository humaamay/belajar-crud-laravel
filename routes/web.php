<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('pertanyaan')->group(function () {
    Route::get('/', 'QuestionController@index')->name('index_pertanyaan');

    Route::post('/', 'QuestionController@store')->name('post_pertanyaan');

    Route::get('/create', 'QuestionController@create')->name('buat_pertanyaan');

    Route::get('{pertanyaan_id}', 'QuestionController@show')->name('detail_pertanyaan');

    Route::get('{pertanyaan_id}/edit', 'QuestionController@edit')->name('edit_pertanyaan');

    Route::put('{pertanyaan_id}', 'QuestionController@update')->name('update_pertanyaan');

    Route::delete('{pertanyaan_id}', 'QuestionController@destroy')->name('hapus_pertanyaan');
});
