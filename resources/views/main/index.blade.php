@extends('layouts.master')
@section('title', 'Pertanyaan')

@section('content')
    @if (count($questions) > 0)
        <!-- ============================================================== -->
        <!-- pagination -->
        <!-- ============================================================== -->
        <div class="d-flex list-inline mx-auto justify-content-center">
            {{ $questions->links() }}
        </div>
        <!-- ============================================================== -->
        <!-- end pagination -->
        <!-- ============================================================== -->
    @endif
    @foreach ($questions as $question)
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-body mx-1">
                    <blockquote class="blockquote mb-4">
                        <h2><a href="{{ route('detail_pertanyaan', $question->id) }}">{{  $question->title }}</a></h2>
                    </blockquote>
                    <span class="question">
                        {{ $question->contents }}
                    </span>
                </div>
            </div>
        </div>
    @endforeach
@endsection