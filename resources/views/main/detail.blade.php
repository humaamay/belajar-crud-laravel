@extends('layouts.master')
@section('title', $question->title)
@section('editme', "/pertanyaan/" . $question->id . "/edit")
@section('delme', "/pertanyaan/" . $question->id)

@section('content')
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
    <div class="card">
        <div class="card-body">
            {!! parsedown($question->contents) !!}
        </div>
    </div>
    
    <h2 class="pageheader-title">Jawaban</h2>
    <hr />
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <h2>Ini adalah contoh jawaban</h2>
                    <span>
                        Ini adalah contoh jawaban
                    </span>
                </div>
            </div>
        </div>
    </div>

    <h2 class="pageheader-title">Jawaban Kamu</h2>
    <hr />
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <form onsubmit="return false;">
                <div class="form-group">
                    <textarea name="text" id="editor"></textarea>
                </div>
                <div class="form-group">
                    <a href="{{ route('index_pertanyaan') }}"><button class="btn btn-brand float-right" type="button">Kembali</button></a>
                    <button class="btn btn-primary float-right mr-3" onclick="return alert('under maintaince')">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('md-editor')
<!-- Markdown Editor -->
<script src="{{  asset('assets/ace/ace.js') }}"></script>
<script src="{{  asset('assets/local/js/md.min.js') }}"></script>
<script src="{{ asset('assets/local/js/md.js') }}"></script>
<script>
$('#editor').markdownEditor({
    preview: true,
    onPreview: function (content, callback) {
        callback( marked(content) );
    }
});
</script>
@endpush