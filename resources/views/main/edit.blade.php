@extends('layouts.master')
@section('title', 'Edit Pertanyaan')

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('update_pertanyaan', $question->id) }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="inputJudul" class="col-form-label">Judul</label>
                        <small class="form-text text-muted">Tulis judul yang merangkum masalah kamu dengan singkat dan jelas. (max: 45)</small>
                        <input name="title" id="inputJudul" type="text" class="form-control @error('title')is-invalid @enderror" placeholder="e.g. Bagaimana cara melakukan migration di laravel 6" value="{{ old('title') ?? $question->title }}">
                        @error('title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputBody">Deskripsi</label>
                        <textarea name="body" id="inputBody">{{ old('body') ?? $question->contents }}</textarea>
                        @error('body')
                            <div class="d-block invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group pt-1">
                        <a href="{{ route('index_pertanyaan') }}/{{ $question->id }}"><button class="btn btn-brand float-right" type="button">Kembali</button></a>
                        <button class="btn btn-primary float-right mr-3">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('md-editor')
<!-- Markdown Editor -->
<script src="{{  asset('assets/ace/ace.js') }}"></script>
<script src="{{  asset('assets/local/js/md.min.js') }}"></script>
<script src="{{ asset('assets/local/js/md.js') }}"></script>
<script>
$('#inputBody').markdownEditor({
    preview: true,
    onPreview: function (content, callback) {
        callback( marked(content) );
    }
});
@error('body')
    $("#md-editor").addClass('border-danger');
@enderror
</script>
@endpush