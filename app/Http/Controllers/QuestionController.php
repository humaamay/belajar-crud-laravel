<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = DB::table('questions')->orderBy('id', 'desc')->paginate(10);
        return view('main.index', ['questions' => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('main.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:45|min:15',
            'body' => 'required'
        ]);

        try {
            DB::table('questions')->insert(
                [
                    'title' => $request->input('title'),
                    'contents' => $request->input('body'),
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                    'best_answer_id' => 0,
                    'profile_id' => 0
                ]
            );
            
            $swal = 'setTimeout(function(){swal({title:"Berhasil",text:"Selamat anda berhasil membuat pertanyaan.",type:"success",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        } catch (Exception $e) {
            $swal = 'setTimeout(function(){swal({title:"Gagal",text:"Anda gagal membuat pertanyaan.",type:"error",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan/create")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $question = DB::table('questions')->where('id', $id)->first();
            if (null == $question) {
                throw new Exception();
            }

            return view('main.detail', ['question' => $question]);
        } catch (Exception $e) {
            $swal = 'setTimeout(function(){swal({title:"Gagal",text:"Pertanyaan tidak ditemukan.",type:"error",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $question = DB::table('questions')->where('id', $id)->first();
            if (null == $question) {
                throw new Exception();
            }
            
            return view('main.edit', ['question' => $question]);
        } catch (Exception $e) {
            $swal = 'setTimeout(function(){swal({title:"Gagal",text:"Pertanyaan tidak ditemukan.",type:"error",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::table('questions')->where('id', $id)->update([
                'title' => $request->input('title'),
                'contents' => $request->input('body'),
                'updated_at' => \Carbon\Carbon::now()
            ]);

            $swal = 'setTimeout(function(){swal({title:"Berhasil",text:"Berhasil mengubah pertanyaan.",type:"success",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan/' . $id . '")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        } catch (Exception $e) {
            $swal = 'setTimeout(function(){swal({title:"Gagal",text:"Gagal mengubah pertanyaan.",type:"error",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan/' . $id . '")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $delete = DB::table('questions')->where('id', $id)->delete();
            if (null == $delete) {
                throw new Exception();
            }

            $swal = 'setTimeout(function(){swal({title:"Berhasil",text:"Anda berhasil menghapus pertanyaan.",type:"success",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        } catch (Exception $e) {
            $swal = 'setTimeout(function(){swal({title:"Gagal",text:"Anda gagal menghapus pertanyaan.",type:"error",allowOutsideClick:!1}).then(function(t){t&&(window.location.href="/pertanyaan/'. $id.'")})},100);';
            return view('layouts.sweetalert', ['swal' => $swal]);
        }
    }
}
